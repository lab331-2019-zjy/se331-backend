package se331.backend.rest.security.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import se331.backend.rest.security.entity.Authority;
import se331.backend.rest.security.entity.AuthorityName;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {
    Authority findByName(AuthorityName input);
}
