package se331.backend.rest.security.entity;

public enum AuthorityName {
    ROLE_STUDENT, ROLE_ADMIN, ROLE_TEACHER
}