package se331.backend.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import se331.backend.rest.mapper.MapperUtil;
import se331.backend.rest.service.TeacherService;

@Controller
public class TeacherController {
    @Autowired
    TeacherService teacherService;

    @GetMapping("/teachers")
    public ResponseEntity getTeachers() {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getTeacherDto(teacherService.getAllTeachers()));
    }

}
