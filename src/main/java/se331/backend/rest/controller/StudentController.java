package se331.backend.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.backend.rest.entity.Student;
import se331.backend.rest.mapper.MapperUtil;
import se331.backend.rest.service.StudentService;


@Controller
public class StudentController {

    @Autowired
    StudentService studentService;

    @GetMapping("/students")
    public ResponseEntity getStudents() {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(studentService.getAllStudents()));
    }

    @GetMapping("/students/{id}")
    public ResponseEntity getStudentById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(studentService.findById(id)));
    }

    @PostMapping("/students")
    public ResponseEntity saveStudent(@RequestBody Student student){
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(studentService.saveStudent(student)));
    }
}
