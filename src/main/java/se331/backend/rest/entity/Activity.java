package se331.backend.rest.entity;


import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity

public class Activity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    String activityId;
    String name;
    String content;
    @ManyToMany
    @Builder.Default
    List<Student> students = new ArrayList<>();
    @ManyToOne
    @JsonBackReference
    Teacher teacher;
    String location;
    Date time;

}
