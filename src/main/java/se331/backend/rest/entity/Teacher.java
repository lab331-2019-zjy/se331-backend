package se331.backend.rest.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Teacher extends Person {

    @OneToMany(mappedBy = "teacher")
    @Builder.Default
    @ToString.Exclude
    List<Activity> activities = new ArrayList<>();
}
