package se331.backend.rest.service;

import se331.backend.rest.entity.Activity;

import java.util.List;

public interface ActivityService {
    List<Activity> getAllActivities();
    Activity findById(long id);
    Activity saveActivity(Activity activity);
}
