package se331.backend.rest.service;

import se331.backend.rest.entity.Student;
import se331.backend.rest.entity.Teacher;

import java.util.List;

public interface TeacherService {
    List<Teacher> getAllTeachers();
    Teacher findById(long id);
    Teacher saveTeacher(Teacher teacher);
}
