package se331.backend.rest.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import se331.backend.rest.entity.Activity;
import se331.backend.rest.entity.Student;
import se331.backend.rest.entity.Teacher;
import se331.backend.rest.repository.ActivityRepository;
import se331.backend.rest.repository.StudentRepository;
import se331.backend.rest.repository.TeacherRepository;
import se331.backend.rest.security.entity.Authority;
import se331.backend.rest.security.entity.AuthorityName;
import se331.backend.rest.security.entity.User;
import se331.backend.rest.security.repository.AuthorityRepository;
import se331.backend.rest.security.repository.UserRepository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;


@Component
public class DataLoader implements ApplicationRunner {
    @Autowired
    StudentRepository studentRepository;
    @Autowired
    TeacherRepository teacherRepository;
    @Autowired
    ActivityRepository activityRepository;

    @Autowired
    AuthorityRepository authorityRepository;
    @Autowired
    UserRepository userRepository;

    @Override
    @Transactional
    public void run(ApplicationArguments args) throws Exception {
        Student student1 = Student.builder().name("Junyu").surname("Zhou").studentId("592115508").image("https://www.mobafire.com/images/avatars/teemo-classic.png").build();
        Student student2 = Student.builder().name("Zihao").surname("Yu").studentId("592115521").image("https://www.mobafire.com/images/avatars/teemo-classic.png").build();
        Student student3 = Student.builder().name("Yibei").surname("Bai").studentId("602115521").image("https://www.mobafire.com/images/avatars/teemo-classic.png").build();

        Teacher teacher1 = Teacher.builder().name("Ball").surname("Aj").image("https://www.googleapis.com/download/storage/v1/b/se331-lab11.appspot.com/o/2019-12-05%20164241801-50706061_2299379266966365_5657599027034718208_n.jpg?generation=1575538962701784&alt=media").build();
        Teacher teacher2 = Teacher.builder().name("Patama").image("https://www.googleapis.com/download/storage/v1/b/se331-lab11.appspot.com/o/2019-12-05%20153342326-78278901_2471902956431013_121625317679824896_n.jpg?generation=1575534823460748&alt=media").surname("Aj").build();

        Activity activity1 = Activity.builder().name("Help kids").content("Help some homeless kids.").activityId("001").build();
        Activity activity2 = Activity.builder().name("Eat lunch").content("Eat lunch with homeless kids").activityId("002").build();

        student1 = studentRepository.save(student1);
        student2 = studentRepository.save(student2);
        student3 = studentRepository.save(student3);

        teacher1 = teacherRepository.save(teacher1);
        teacher2 = teacherRepository.save(teacher2);

        activity1 = activityRepository.save(activity1);
        activity2 = activityRepository.save(activity2);

        activity1.getStudents().add(student1);
        activity1.getStudents().add(student2);
        activity2.getStudents().add(student3);

        activity1.setTeacher(teacher1);
        teacher1.getActivities().add(activity1);

        activity2.setTeacher(teacher2);
        teacher2.getActivities().add(activity2);

        PasswordEncoder encoder = new BCryptPasswordEncoder();

        Authority auth1 = Authority.builder().name(AuthorityName.ROLE_TEACHER).build();
        Authority auth2 = Authority.builder().name(AuthorityName.ROLE_STUDENT).build();
        Authority auth3 = Authority.builder().name(AuthorityName.ROLE_ADMIN).build();

        User user1, user2, user3;
        user1 = User.builder()
                .username("admin")
                .password(encoder.encode("admin"))
                .firstname("admin")
                .lastname("admin")
                .email("admin@admin.com")
                .enabled(true)
                .lastPasswordResetDate(Date.from(LocalDate.of(2019,1 ,1).atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .build();

        user2 = User.builder()
                .username("student")
                .password(encoder.encode("123456"))
                .firstname("student")
                .lastname("student")
                .email("student@cmu.ac.th")
                .enabled(true)
                .lastPasswordResetDate(Date.from(LocalDate.of(2019,1 ,1).atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .build();


        user3 = User.builder()
                .username("teacher")
                .password(encoder.encode("123456"))
                .firstname("teacher")
                .lastname("teacher")
                .email("teacher@cmu.ac.th")
                .enabled(true)
                .lastPasswordResetDate(Date.from(LocalDate.of(2019,1 ,1).atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .build();

        authorityRepository.save(auth1);
        authorityRepository.save(auth2);
        authorityRepository.save(auth3);

        //admin

        user1.getAuthorities().add(auth3);

        //student
        user2.getAuthorities().add(auth2);

        //teacher
        user3.getAuthorities().add(auth1);


        //set teacher
        teacher1.setUser(user1);
        teacher2.setUser(user3);

        //set student
        student1.setUser(user2);


        userRepository.save(user1);
        userRepository.save(user2);
        userRepository.save(user3);

    }
}
