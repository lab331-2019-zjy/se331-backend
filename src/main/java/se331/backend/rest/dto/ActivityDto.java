package se331.backend.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import se331.backend.rest.entity.Student;

import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ActivityDto {
    Long id;
    String name;
    String content;
    String location;
    Date time;
    List<Student> students;

}
