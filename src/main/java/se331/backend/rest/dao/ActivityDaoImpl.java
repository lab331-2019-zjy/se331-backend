package se331.backend.rest.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import se331.backend.rest.entity.Activity;
import se331.backend.rest.entity.Student;
import se331.backend.rest.repository.ActivityRepository;
import se331.backend.rest.repository.StudentRepository;

import java.util.List;

@Slf4j
@Profile("Dao")
@Repository
public class ActivityDaoImpl implements ActivityDao {

    @Autowired
    ActivityRepository activityRepository;
    @Override
    public List<Activity> getAllActivities() {
        log.info("Find all activities from database.");
        return activityRepository.findAll();
    }

    @Override
    public Activity findById(long id) {
        log.info("Find activity with id {} from database.", id);
        return activityRepository.findById(id);
    }

    @Override
    public Activity saveActivity(Activity activity) {
        log.info("Save activity with id {} to database.", activity);
        return activityRepository.save(activity);
    }
}
