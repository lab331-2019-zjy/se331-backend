package se331.backend.rest.dao;

import se331.backend.rest.entity.Student;

import java.util.List;

public interface StudentDao {
    List<Student> getAllStudents();
    Student findById(long id);
    Student saveStudent(Student student);
}
