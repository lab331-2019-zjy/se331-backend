package se331.backend.rest.dao;

import se331.backend.rest.entity.Activity;

import java.util.List;

public interface ActivityDao {
    List<Activity> getAllActivities();
    Activity findById(long id);
    Activity saveActivity(Activity activity);
}
